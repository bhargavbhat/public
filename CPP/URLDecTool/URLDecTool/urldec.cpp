#include<iostream>
#include<string>

using std::cout;
using std::endl;
using std::cin;

char* decodePercents(const char* inputURL, const unsigned long inputSize)
{
	char* retURL = new char[inputSize];
	memset(retURL, 0, inputSize);

	//Temp. handles for string manip
	const char* inputStr = inputURL;
	char* decodedURL = retURL;

	//Placeholders for the hex digits + terminating NULL
	char hexDigits[3] = {0};
	long asciiValue = 0;

	while(*inputStr)
	{
		//Check for encoded chars
		if(*inputStr == '%')
		{
			strncpy_s(hexDigits, (inputStr+1), 2);
			asciiValue = strtol(hexDigits,NULL,16);

			//Check if char is in valid ASCII range
			*decodedURL++ = (asciiValue >=32) && (asciiValue <=127) ? static_cast<char>(asciiValue) : ' ';

			inputStr += 3;
		}
		else
		{
			//Copy directly to destination
			*(decodedURL++) = *(inputStr++);
		}
	}
	
	return retURL;
}
int main(int argc, char** argv)
{
	if(argc != 1)
	{
		cout<<"Command-line parameters passed-in will be ignored."<<endl;
	}

	cout<<"Hit CTRL+C or CTRL+BREAK to quit"<<endl;

	std::string inputURL;
	char* decodedURL = NULL;

	while(1)
	{
		cout<<"Input: ";
		cin>>inputURL;
		decodedURL = decodePercents(inputURL.c_str(), inputURL.size());
		cout<<"Output: "<<decodedURL<<endl;
		if(decodedURL)
		{
			delete[] decodedURL;
			decodedURL = NULL;
		}
	}

	//We're done, return success code back to the OS
	return EXIT_SUCCESS;

}