Title	: URLDecTool

Date	: 13-Oct-2012

Desc	: Simple program for testing a function written to decode "percentage encoded" URLs.

Lang	: C/C++

Compiler: Visual Studio 2012 Express for Desktop	

Ref 	: https://en.wikipedia.org/wiki/Percent-encoding